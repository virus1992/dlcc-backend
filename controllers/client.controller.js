/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const sendResponse = require('../modules/response.helper');
var mailer = require('../modules/mailer');
var constants = require('../modules/constants');
var clientService = require('../services/client.service');

var searchClients = function (req, res, next) {
    clientService.getClientFromKeyWord(req, function (clients, error) {
        if (clients) {
            return sendResponse.sendJsonResponse(req, res, 200, clients, "", "List of Clients");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, error);
        }
    });
};

exports.searchClients = searchClients;



