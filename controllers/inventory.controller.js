/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const sendResponse = require('../modules/response.helper');
var mailer = require('../modules/mailer');
var constants = require('../modules/constants');
var orderService = require('../services/order.service');
var transactionService = require('../services/transaction.service');
var inventoryService = require('../services/inventory.service');
var getInventory = function (req, res, next) {
    inventoryService.getAllInventories(req, function (inventories) {
        if (inventories != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, inventories, "", "inventory listing!!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};
exports.getInventory = getInventory;

var editInventory = function (req, res, next) {
    inventoryService.updateInventory(req, function (inventory) {
        if (inventory != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, {}, "", "inventory updated successfully!!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};
exports.editInventory = editInventory;


var getCryptocurrency = function (req, res, next) {
    inventoryService.getAllCryptocurrency(req, function (cryptocurrencies) {
        if (cryptocurrencies != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, cryptocurrencies, "", "List of cryptocurrencies");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, error);
        }
    });
};

exports.getCryptocurrency = getCryptocurrency;

var recallInventory = function (req, res, next) {
    inventoryService.recallCurrentInventory(req, function (inventory) {
        if (inventory != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, {}, "", "Inventory Recalled");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, error);
        }
    });
};

exports.recallInventory = recallInventory;