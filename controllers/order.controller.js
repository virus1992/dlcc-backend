/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const sendResponse = require('../modules/response.helper');
var mailer = require('../modules/mailer');
var constants = require('../modules/constants');
var orderService = require('../services/order.service');
var transactionService = require('../services/transaction.service');
var alertService = require('../services/alert.service');

var locateOrder = function (req, res, next) { 
    transactionService.addNewTransaction(req, function (transaction) {
        if (transaction != "err") {
            var alerttype = 'order-located';
            alertService.addNewAlert(req, alerttype, transaction, function (err, alert) {
                if (alert) {
                    return sendResponse.sendJsonResponse(req, res, 200, {'transactionId': transaction._id}, "", "order located successfully");
                } else {
                    return sendResponse.sendJsonResponse(req, res, 201, {}, err);
                }
            });

        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};

exports.locateOrder = locateOrder;


var getOpenOrders = function (req, res, next) {
    orderService.getAllOpenOrders(req, function (orders) {
        if (orders != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, orders, "", "open orders list!!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};

exports.getOpenOrders = getOpenOrders;


var getOpenPositions = function (req, res, next) {
    orderService.getAllOpenPositions(req, function (orders) {
        if (orders != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, orders, "", "open positions list!!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};

exports.getOpenPositions = getOpenPositions;

var getPositions = function (req, res, next) {
    orderService.getAllPositions(req, function (positions) {
        if (positions != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, positions, "", "positions list!!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};

exports.getPositions = getPositions;

var cancleOpenOrder = function (req, res, next) {
    orderService.cancleCurrentOpenOrder(req, function (order) {
        if (order != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, {}, "", "Open order cancelled!!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};

exports.cancleOpenOrder = cancleOpenOrder;

var editOpenOrder = function (req, res, next) {
    orderService.editCurrentOpenOrder(req, function (order) {
        if (order != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, {}, "", "Open order updated!!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};

exports.editOpenOrder = editOpenOrder;

var closePosition = function (req, res, next) {
    orderService.closeCurrentPosition(req, function (order) {
        if (order != "err") {
            return sendResponse.sendJsonResponse(req, res, 200, {}, "", "Closed positions!!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};

exports.closePosition = closePosition;

var executeOrder = function (req, res, next) {

    transactionService.executeCurrentOrder(req, function (execute) {
        if (execute != "err") {
            var alerttype = 'order-executed';
            alertService.addExecuteAlert(req, alerttype, execute, function (err, alert) {
                if (alert) {
                    return sendResponse.sendJsonResponse(req, res, 200, {}, "", "order executed successfully");
                } else {
                    return sendResponse.sendJsonResponse(req, res, 201, {}, err);
                }
            });
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went Wrong");
        }
    });
};

exports.executeOrder = executeOrder;