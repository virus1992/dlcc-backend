/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const sendResponse = require('../modules/response.helper');
var mailer = require('../modules/mailer');
var constants = require('../modules/constants');
var alertService = require('../services/alert.service');


var getAlerts = function (req, res, next) {
    alertService.getAllAlert(req, function (alerts) {
        if (alerts != 'error') {
            return sendResponse.sendJsonResponse(req, res, 200, alerts, "", "List of Alerts!");
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "Something went wrong!");
        }
    });
};

exports.getAlerts = getAlerts;