var expressMailer = require('express-mailer');

var sendMail = function (req, res, mailParamsObject, callback) {

    var mailerOptions = {
        to: mailParamsObject.to,
        subject: mailParamsObject.subject
    };

    var templateVariable = mailParamsObject.templateVariable;

    var finalMailerOptions = JSON.parse((JSON.stringify(mailerOptions) + JSON.stringify(templateVariable)).replace(/}{/g, ","))
    var template = mailParamsObject.templateVariable.templateURL;
    req.app.mailer.send(template, finalMailerOptions, function (err) {
        if (err) {
            callback(err);
        }
        callback(null);
    });
}
exports.sendMail = sendMail;