/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const express = require('express');
const router = express.Router();
const orderController = require('../../controllers/order.controller');
const clientController = require('../../controllers/client.controller');
const alertController = require('../../controllers/alert.controller');
const inventoryController = require('../../controllers/inventory.controller');


router.post('/locateOrder', orderController.locateOrder);//k
router.post('/executeOrder', orderController.executeOrder);//k
router.post('/getOpenOrders', orderController.getOpenOrders);//k
router.post('/cancleOpenOrder', orderController.cancleOpenOrder);//k
router.post('/getOpenPositions', orderController.getOpenPositions);
router.post('/getPositions', orderController.getPositions);
router.post('/closePosition', orderController.closePosition);
router.post('/editOpenOrder', orderController.editOpenOrder);
router.post('/searchClients', clientController.searchClients);//k
router.post('/getAlerts', alertController.getAlerts);//k
router.post('/getInventory', inventoryController.getInventory);
router.post('/editInventory', inventoryController.editInventory);
router.post('/recallInventory', inventoryController.recallInventory);
router.get('/getCryptocurrency', inventoryController.getCryptocurrency);//k
module.exports = router;
