/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inventory = require('../models/inventories');
var lenders = require('../models/lenders');
var crypto = require('../models/cryptocurrency');

exports.getAllInventories = function (req, callback) {
    inventory.find({isDeleted: false}).populate('symbol', 'symbol').populate('lender', 'name').lean().exec(function (err, inventories) {

        if (err) {
            callback("err");
        } else {
            callback(inventories);
        }
    });
//    inventory.aggregate([
//        {$match: {'isDeleted': false}},        
//        {$lookup: {from: "cryptocurrency", localField: "symbol", foreignField: "_id", as: "symbol"}},        
//        {$lookup: {from: "lenders", localField: "lender", foreignField: "_id", as: "lender"}},                
//        {$unwind: {path: '$symbol', "preserveNullAndEmptyArrays": true}},
//        {$unwind: {path: '$lenders', "preserveNullAndEmptyArrays": true}},
//        {$project: { 'symbol._id': 1, 'symbol.symbol': 1,'lender._id': 1, 'lender.name': 1,_id: 1,quantity: 1, rate: 1}},
//        {"$group": {"_id": "$symbol",inventories: {$push: {_id: '$_id',quantity: '$quantity', rate: '$rate', 'lender': "$lender.name"}}}},                
//    ], function (err, result) {
//        if (err) {
//            console.log('error.................', err);
//            callback('err');
//        } else {
//            console.log(result);
//            callback(result);
//        }
//    })
};

exports.getAllCryptocurrency = function (req, callback) {
    crypto.find().select('symbol rate price').lean().exec(function (err, cryptocurrencies) {
        if (err) {
            callback("err");
        } else {
            callback(cryptocurrencies);
        }
    });
};


exports.updateInventory = function (req, callback) {
    inventory.update({_id: req.body.inventory_id}, {$set: {quantity: req.body.quantity, rate: req.body.rate}}).exec(function (err, invUp) {
        if (err) {
            callback("err");
        } else {
            callback(invUp);
        }
    });
};

exports.recallCurrentInventory = function (req, callback) {
    inventory.update({_id: req.body.inventory_id}, {$set: {isDeleted: true}}).exec(function (err, invUp) {
        if (err) {
            callback("err");
        } else {
            callback(invUp);
        }
    });
};

