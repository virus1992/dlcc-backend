/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var alerts = require('../models/alerts');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var moment = require('moment');

exports.addNewAlert = function (req, alerttype, transaction, callback) {
    var newAlert = {
        'alertType': alerttype,
        'alert': '<div class="box"><h4>XYZ<br>' + req.body.quantity + ' ' + transaction.symbol.symbol + ' located ' + moment(transaction.createdAt).format('LLLL') + ' </h4><div class="confirm-btn"><a href="#" class="btn btn-primary" id="viewLocate">View All Locates</a></div></div>',
        'clientId': req.body.client_id
    };

    alerts(newAlert).save(function (err, saveAlert) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, saveAlert);
        }
    });
};
exports.addExecuteAlert = function (req, alerttype, transaction, callback) {
    var newAlert = {
        'alertType': alerttype,
        'alert': '<div class="box"><h4> Client ' + transaction.client.fullname + '<br> Sold Short ' + transaction.quantity + ' ' + transaction.symbol.symbol + ' at $' + transaction.limitPrice + ' at rate ' + transaction.rate + '%</h4></div>',
        'clientId': transaction.client._id
    };

    alerts(newAlert).save(function (err, saveAlert) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, saveAlert);
        }
    });
};


exports.getAllAlert = function (req, callback) {
    alerts.find().populate('clientId', 'fullname').lean().exec(function (err, allAlerts) {
        if (err) {
            callback("error");
        } else {
            callback(allAlerts);
        }
    });
};