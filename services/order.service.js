/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var openOrders = require('../models/open_orders');
var openPositions = require('../models/open_positions');
var positions = require('../models/positions');

exports.getAllOpenOrders = function (req, callback) {
    openOrders.find({isDeleted: false}).populate('symbol', 'symbol').populate('client', 'fullname').lean().exec(function (err, orders) {
        if (err) {
            callback("err");
        } else {
            callback(orders);
        }
    });
};
exports.getAllOpenPositions = function (req, callback) {
    openPositions.find({isClosed: false}).populate('symbol', 'symbol').populate('client', 'fullname').lean().exec(function (err, positions) {
        if (err) {
            callback("err");
        } else {
            callback(positions);
        }
    });
};
exports.getAllPositions = function (req, callback) {
    positions.find().populate('symbol', 'symbol').lean().exec(function (err, positions) {
        if (err) {
            callback("err");
        } else {
            callback(positions);
        }
    });
};
exports.cancleCurrentOpenOrder = function (req, callback) {
    openOrders.update({_id: req.body.open_order_id}, {$set: {isDeleted: true}}).exec(function (err, order) {
        if (err) {
            callback("err");
        } else {
            callback(order);
        }
    });
};

exports.editCurrentOpenOrder = function (req, callback) {
    if (req.body.order_type == "market") {
        openOrders.findOneAndUpdate({_id: req.body.open_order_id}, {$set: {isDeleted: true}}, {new : true}).exec(function (err, order) {
            if (err) {
                callback("err");
            } else {
                var newOpenPosition = {
                    symbol: order.symbol,
                    client: order.client,
                    avgRate: order.rate,
                    avgPrice: order.limit,
                    quantity: req.body.quantity,
                };

                openPositions(newOpenPosition).save(function (err, openPosition) {
                    if (err) {
                        callback("err");
                    } else {
                        callback(openPosition);
                    }
                })
            }
        });
    } else {
        openOrders.findOneAndUpdate({_id: req.body.open_order_id}, {$set: {quantity: req.body.quantity, limit: req.body.limit}}, {new : true}).exec(function (err, order) {
            if (err) {
                callback("err");
            } else {
                callback(order);
            }
        });
    }
};

exports.closeCurrentPosition = function (req, callback) {
    openPositions.findOneAndUpdate({_id: req.body.position_id}, {$set: {isClosed: true}}, {new : true}).exec(function (err, order) {
        if (err) {
            callback("err");
        } else {
            callback(order);
        }
    });
};


exports.addNewOrder = function (transaction, callback) {
    if (transaction.orderType == "limit") {
        var newOpenOrder = {
            client: transaction.client._id,
            symbol: transaction.symbol._id,
            limit: transaction.limitPrice,
            rate: transaction.rate,
            quantity: transaction.quantity
        };
        openOrders(newOpenOrder).save(function (err, neworder) {
            if (err) {
                callback("err");
            } else {
                callback(neworder);
            }
        });

    } else {
        var newPostionOrder = {
            client: transaction.client._id,
            symbol: transaction.symbol._id,
            avgPrice: transaction.limitPrice,
            avgRate: transaction.rate,
            quantity: transaction.quantity
        };
        openPositions(newPostionOrder).save(function (err, newposition) {
            if (err) {
                callback("err");
            } else {
                callback(newposition);
            }
        });
    }
};  