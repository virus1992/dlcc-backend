/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');

var transactions = require('../models/transactions');
var cryptocurrency = require('../models/cryptocurrency');
var orderService = require('../services/order.service');
exports.addNewTransaction = function (req, callback) {
    var newTransaction = {
        'symbol': req.body.symbol_id,
        'client': req.body.client_id,
        'quantity': req.body.quantity,
        'orderStatus': 'located'
    };

    transactions(newTransaction).save(function (err, saveTransaction) {
        if (err) {
            callback("err");
        } else {
            transactions.findOne({_id: saveTransaction._id}).populate('client', 'fullname').populate('symbol', 'symbol').lean().exec(function (err, result) {
                
                if (err) {

                    callback("err");
                } else {
                    callback(result);
                }
            });
        }
    });
};
exports.executeCurrentOrder = function (req, callback) {
    transactions.findOneAndUpdate({_id: req.body.transaction_id}, {$set: {rate: req.body.rate, orderType: req.body.order_type, limitPrice: req.body.limit_price, orderStrategy: req.body.order_strategay, orderStatus: 'executed'}}, {new : true}).populate('client', 'fullname').populate("symbol", "symbol").lean().exec(function (error, transaction) {
    if (error) {
            callback("err");
        } else {
            orderService.addNewOrder(transaction, function (orderAdd) {
                if (orderAdd != "err") {
                    callback(transaction);
                } else {
                    callback("err");
                }
            });
        }
    });
};



