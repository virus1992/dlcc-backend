/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var clients = require('../models/clients');

exports.getClientFromKeyWord = function (req, callback) {
    clients.find({fullname: {$regex: new RegExp(req.body.keyword, "i")}}).select('fullname _id').exec(function (error, success) {
        if (success) {
            callback(success, null);
        } else {
            callback(null, error);
        }
    });
};
