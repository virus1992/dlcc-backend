var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var session = require('express-session');
var constants = require('./modules/constants');
var exphbs = require('express-handlebars');
var mailer = require('express-mailer');
var handleBarHelpers = require('./modules/handlebarhelpers');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var apiRoutes = require('./routes/api.route');

var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('./swagger.json');
var app = express();
app.use(session({
    secret: 'dlcc-web',
    resave: false,
    saveUninitialized: true,
    cookie: {
        path: '/',
        httpOnly: false,
        secure: false,
        maxAge: 31536000000
    }
}));

mailer.extend(app, {
    from: constants.MAIL_FROM,
    host: constants.MAIL_HOST, // hostname 
    secureConnection: constants.MAIL_SECURE, // use SSL 
    port: constants.MAIL_PORT, // port for secure SMTP 
    transportMethod: constants.MAIL_METHOD, // default is SMTP. Accepts anything that nodemailer accepts 
    auth: {
        user: constants.MAIL_FROM_AUTH,
        pass: constants.MAIL_PASSWORD
    }
});

var hbs = exphbs.create({
    defaultLayout: 'blank',
    // Uses multiple partials dirs, templates in "shared/templates/" are shared
    // with the client-side of the app (see below).
    partialsDir: [
        'views/partials/'
    ],
    helpers: handleBarHelpers.helperFunction
});

mongoose.set("debug", (collectionName, method, query, doc) => {
    console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
});


//mongoose.set("debug", true);
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/dlcc', {useNewUrlParser: true})
        .then(() => {
            console.log(`Succesfully Connected to the Mongodb Database at URL : mongodb://localhost:27017/dlcc`)
        })
        .catch(() => {
            console.log(`Error Connecting to the Mongodb Database at URL : mongodb://localhost:27017/dlcc`)
        });



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// set headers
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
