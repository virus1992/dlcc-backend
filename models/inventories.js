/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var InventoriesSchema = new mongoose.Schema({
    symbol: {type: Schema.Types.ObjectId, ref: 'cryptocurrency'},
    lender: {type: Schema.Types.ObjectId, ref: 'lenders'},
    quantity: {type: Number, default: "", required: false},
    rate: {type: Schema.Types.Double, default: "", required: false},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'inventories'
});
InventoriesSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('inventories', InventoriesSchema);


