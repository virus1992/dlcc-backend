/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var BorrowHistorySchema = new mongoose.Schema({
    symbol: {type: Schema.Types.ObjectId, ref: 'cryptocurrency'},
    client: {type: Schema.Types.ObjectId, ref: 'clients'},
    lender: {type: Schema.Types.ObjectId, ref: 'lenders'},
    size: {type: Number, default: ""},
    rate: {type: Number, default: ""},
    executionPrice: {type: Number, default: ""},
    dateTime: {type: Number, default: _.now()},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'borrow_history'
});
BorrowHistorySchema.plugin(mongoosePaginate);
module.exports = mongoose.model('borrow_history', BorrowHistorySchema);


