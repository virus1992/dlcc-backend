/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var PositionsSchema = new mongoose.Schema({
    symbol: {type: Schema.Types.ObjectId, ref: 'cryptocurrency'},
    securityDescription: {type: String, default: ""},
    quantity: {type: Number, default: ""},
    price: {type: Schema.Types.Double, default: ""},
    rate: {type: Schema.Types.Double, default: ""},
    maretValue: {type: Schema.Types.Double, default: ""},
    marginRequirements: {type: Schema.Types.Double, default: ""},
    marginRequirement: {type: Schema.Types.Double, default: ""},
    liquidateForMarginCall: {type: Schema.Types.Double, default: ""},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'positions'
});
PositionsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('positions', PositionsSchema);


