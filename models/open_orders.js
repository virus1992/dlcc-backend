/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var OpenOrdersSchema = new mongoose.Schema({
    symbol: {type: Schema.Types.ObjectId, ref: 'cryptocurrency'},
    client: {type: Schema.Types.ObjectId, ref: 'clients'},
    quantity: {type: Number, default: ""},
    rate: {type: Number, default: ""},
    limit: {type: Number, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'open_orders'
});
OpenOrdersSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('open_orders', OpenOrdersSchema);


