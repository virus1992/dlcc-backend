/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var CryptocurrencySchema = new mongoose.Schema({
    symbol: {type: String, default: ""},
    rate: {type: Schema.Types.Double, default: "",required: false},
    price: {type: Schema.Types.Double, default: "",required: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'cryptocurrency'
});
CryptocurrencySchema.plugin(mongoosePaginate);
module.exports = mongoose.model('cryptocurrency', CryptocurrencySchema);


