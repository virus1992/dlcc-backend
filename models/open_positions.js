/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var OpenPositionSchema = new mongoose.Schema({
    symbol: {type: Schema.Types.ObjectId, ref: 'cryptocurrency'},
    client: {type: Schema.Types.ObjectId, ref: 'clients'},
    quantity: {type: Number, default: ""},
    avgRate: {type: Schema.Types.Double, default: ""},
    avgPrice: {type: Schema.Types.Double, default: ""},
    isDeleted: {type: Boolean, default: false},
    isClosed: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'open_position'
});
OpenPositionSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('open_position', OpenPositionSchema);


