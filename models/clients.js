/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var ClientsSchema = new mongoose.Schema({
    fullname: {type: String, default: ""},
    email: {type: String, default: ""},
    password: {type: Number, default: ""},
    isActive: {type: Boolean, default: true},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'clients'
});
ClientsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('clients', ClientsSchema);


