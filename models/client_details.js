/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var ClientDetailsSchema = new mongoose.Schema({
    clientId: {type: Schema.Types.ObjectId, ref: 'clients'},
    netMktValue: {type: Number, default: ""},
    lmv: {type: Number, default: ""},
    smv: {type: Number, default: ""},
    cash: {type: Number, default: ""},
    totalMarginRequiremnt: {type: Number, default: ""},
    marginExcess: {type: Number, default: ""},
    buyingPower: {type: Number, default: ""},
    marginTimerClock: {type: Number, default: ""},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'client_details'
});
ClientDetailsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('client_details', ClientDetailsSchema);


