/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;

var TransactionsSchema = new mongoose.Schema({
    symbol: {type: Schema.Types.ObjectId, ref: 'cryptocurrency', required: false},
    client: {type: Schema.Types.ObjectId, ref: 'clients', required: false},
    lender: {type: Schema.Types.ObjectId, ref: 'lenders', required: false},
    quantity: {type: Number, default: "", required: false},
    rate: {type: Schema.Types.Double, default: "", required: false},
    orderType: {type: String, default: "", required: false},
    limitPrice: {type: Schema.Types.Double, default: "", required: false},
    orderStrategy: {type: String, default: "", required: false},
    orderStatus: {type: String, default: "", required: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'transactions'
});
TransactionsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('transactions', TransactionsSchema);


