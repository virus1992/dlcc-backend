/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var LendersSchema = new mongoose.Schema({
    name: {type: String, default: ""},
    quantity: {type: Number, default: ""},
    rate: {type: Number, default: ""},
    price: {type: Number, default: ""},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'lenders'
});
LendersSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('lenders', LendersSchema);


