/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var AlertsSchema = new mongoose.Schema({
    alertType: {type: String, default: ""},
    alert: {type: String, default: ""},
    clientId: {type: Schema.Types.ObjectId, ref: 'clients', required: false},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'alerts'
});
AlertsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('alerts', AlertsSchema);


